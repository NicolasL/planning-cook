const express = require('express');
const router = express.Router();
const request = require('request');
const mongoose = require('mongoose');
const Recipe = require('../models/recipe.js')


router.get('/ingredient', (req, res)=> {

	const arrayIngredient = []
	mongoose.connect('mongodb://localhost/planningCook', function(err) {
		if (err) { throw err; }
	});	
	
	Recipe.find({}, (err, result)=>{
		if (err) throw err
		console.log(result)
		result.forEach((recipe)=>{
			recipe.ingredients.forEach((produits)=>{
				arrayIngredient.push(produits.ingredient)
			})
		})
		res.json(arrayIngredient)
		mongoose.connection.close();
	})
})

router.post('/recipe', (req, res)=>{
	const select = req.body.produit
	const arrayRecipe = []
	mongoose.connect('mongodb://localhost/planningCook', function(err) {
		if (err) { throw err; }
	});	

	/*Recipe.find({ ingredients: { $elemMatch: {ingredient: select}}}, (err, result)=>{
		res.json(result)
		mongoose.connection.close();
	})*/
	Recipe.find({ ingredients: {ingredient: { $regex: /`${select}`/}}}, (err, result)=>{
		res.json(result)
		mongoose.connection.close();
	})
})

module.exports = router;

router.post('/addRecipe', (req, res)=>{
	mongoose.connect('mongodb://localhost/planningCook', function(err) {
		if (err) { throw err; }
	});	

	let new_recipe = new Recipe()
	new_recipe.title = req.body.recipe.title
	new_recipe.ingredients = req.body.recipe.ingredients
	new_recipe.step = req.body.recipe.step
	new_recipe.accompagnement = req.body.recipe.accompagnement
	new_recipe.save(err=>{
		if (err) throw err
		console.log('recipe added')
		mongoose.connection.close();
	})
})
module.exports = router;