'use strict';

let mongoose = require('mongoose');

let recipeSchema = new mongoose.Schema({
    title: String,
    ingredients: Array,
    step: Array,
    accompagnement: Array
})

module.exports = mongoose.model('recipes', recipeSchema);  