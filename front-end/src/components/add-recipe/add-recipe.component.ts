import { Component, OnInit, OnDestroy } from '@angular/core';
import { AddRecipeService } from './add-recipe.service'
import { NgForm } from '@angular/forms';
import {Observable} from 'rxjs/Observable';

@Component({
	selector: 'addrecipe',
	templateUrl: './add-recipe.component.html',
	styleUrls: ['./add-recipe.component.css']
})
export class AddRecipeComponent implements OnInit, OnDestroy{
    
    
    public arrayIngredient= [];
    public title : string
    public ingredient: string;
    public amount: string;
    public selectedValue: string;
    public step: string;
    public accompaniments: string;
    public arrayUnity : string[];
    public arrayStep : any[];
    public arrayAccompaniments: string[];
    public showIngredient: boolean;
    public showStep: boolean;
    public showAccompaniments: boolean
	constructor(public addrecipeservice:AddRecipeService){}

	
	ngOnInit(){
        this.showIngredient = false;
        this.showStep = false;
        this.showAccompaniments = false;
        this.title = '';
        this.ingredient = '';
        this.amount = '';
        this.selectedValue = '';
        this.step = '';
        this.accompaniments = '';
        this.arrayUnity = ['cc', 'cs', 'g', 'cl', 'unit'];
        this.arrayStep = []
        this.arrayAccompaniments = []
    }
    
    addIngredient(){
        
        let newIngredient = {
            ingredient: this.ingredient,
            amount:Number(this.amount),
            unity: this.selectedValue
        }
        this.arrayIngredient.push(newIngredient)
        this.ingredient =''
        this.amount= ''
        this.selectedValue=''

        this.showIngredient=true
    }

    addStep(){
        let newStep = {
            step: this.arrayStep.length+1,
            text: this.step
        }
        this.arrayStep.push(newStep)
        this.step = ''
        this.showStep=true
    }

    addAccompaniments(){
        this.arrayAccompaniments.push(this.accompaniments);
        this.accompaniments = '';
        this.showAccompaniments = true;
    }

    removeIngredient(x){
       const index = x.target.parentElement.parentElement.rowIndex
       this.arrayIngredient.splice(index-1, 1)
       if(this.arrayIngredient.length === 0){
           this.showIngredient = false
       }
    }

    addRecipe(){

        const recipe = {
            title: this.title,
            ingredients : this.arrayIngredient,
            step: this.arrayStep,
            accompagnement: this.arrayAccompaniments
        }
        this.addrecipeservice.addrecipe(recipe).subscribe(
            result=> {
                console.log(result)
            }
        )
    }

	ngOnDestroy(){

	}

}