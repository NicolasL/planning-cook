import { Observable } from "rxjs/Rx"
import { Injectable } from "@angular/core"
import { Http, Response, Headers } from "@angular/http"
import {HttpModule, RequestOptions} from '@angular/http';

import 'rxjs/add/operator/map';


@Injectable()
export class AddRecipeService {
    
    constructor(
        public http: Http
    ){}

    addrecipe(data){
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        const body = {recipe: data}
        return this.http.post('/addRecipe',body,options).map((res: Response)=>res.json())
    }
}