import { Observable } from "rxjs/Rx"
import { Injectable } from "@angular/core"
import { Http, Response, Headers } from "@angular/http"
import {HttpModule, RequestOptions} from '@angular/http';

import 'rxjs/add/operator/map';


@Injectable()
export class SearchService {
    
    constructor(
        public http: Http
    ){}

    getRecipe(){
        return this.http.get('/ingredient').map((res: Response)=>res.json())
    }

    postRecipe(data){
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        const body = {produit: data}
        return this.http.post('/recipe',body,options).map((res: Response)=>res.json())
    }
}


