import { Component, OnInit, OnDestroy } from '@angular/core';
import { SearchService } from './search.service'
import { RecipeListService } from '../recipe-list/recipe-list.service'
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { Subscription } from "rxjs/Subscription";

@Component({
	selector: 'search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit, OnDestroy{
	
	public arrayIngredient: any[];
	public model: any;
	public subscription: Subscription;


	constructor(
		public searchservice:SearchService, 
		public recipelistservice: RecipeListService,
	){}

	
	ngOnInit(){
		this.searchservice.getRecipe()
		.subscribe(
			(data)=>{
				console.log(data)
				this.arrayIngredient = data;
			(err)=>{
				if (err) throw err;
			}
		})
	}

	search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 2 ? []
        : this.arrayIngredient.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10));
	
	onsubmit(value){
		if(value !== undefined)
		this.subscription = this.searchservice.postRecipe(value).subscribe(
			res => {
				this.recipelistservice.sendData(res)
			}
		)
	}	
	
	ngOnDestroy(){
		if(this.subscription){
			this.subscription.unsubscribe()
		}
	}

}
