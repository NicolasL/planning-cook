import { Component, OnInit, OnDestroy } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { RecipeListService } from './recipe-list.service'
import { Subscription } from "rxjs/Subscription";

@Component({
	selector: 'recipelist',
	templateUrl: './recipe-list.component.html',
	styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy{
	
    public arrayRecipe: any[];
    public subscription: Subscription;

	constructor(
        public recipelistservice: RecipeListService
    ){}

	ngOnInit(){
		this.subscription = this.recipelistservice.getData().subscribe(
            res=>{
                this.arrayRecipe = res
            }
        )
    }
    
    selectRecipe(value){
        console.log(value.arrayRecipe[0]._id)
    }

	ngOnDestroy(){
		this.subscription.unsubscribe()
	}

}
