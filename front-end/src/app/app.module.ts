import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app.routing.module'

import { AppComponent } from './app.component'
import { SearchComponent } from '../components/search/search.component'
import { AddRecipeComponent } from '../components/add-recipe/add-recipe.component'
import { NavBarComponent } from '../components/nav-bar/nav-bar.component'
import { HomeComponent } from '../components/home/home.component'
import { RecipeListComponent } from '../components/recipe-list/recipe-list.component'
import { SearchRecipeComponent } from '../components/search-recipe/search-recipe.component'


import { SearchService } from '../components/search/search.service'
import { AddRecipeService } from '../components/add-recipe/add-recipe.service'
import { RecipeListService } from '../components/recipe-list/recipe-list.service'


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    AddRecipeComponent,
    NavBarComponent,
    HomeComponent,
    RecipeListComponent,
    SearchRecipeComponent
  ],

  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    FormsModule,
  ],

  providers: [SearchService, AddRecipeService, RecipeListService],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
