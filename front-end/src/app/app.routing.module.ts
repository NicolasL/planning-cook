import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddRecipeComponent } from '../components/add-recipe/add-recipe.component';
import { SearchRecipeComponent } from '../components/search-recipe/search-recipe.component'
import { HomeComponent } from '../components/home/home.component';

const routes: Routes = [
	{ 	
		path: 'home', 
		component: HomeComponent 
	},
	{ 	
		path: 'add-recipe', 
		component: AddRecipeComponent 
	},
	{ 	
		path: 'search', 
		component: SearchRecipeComponent
	},
	{ 	
		path: '',
		redirectTo : '/home',
		pathMatch: 'full'
	},
]

@NgModule({
	imports: [
		RouterModule.forRoot(routes) 
	],
  	exports: [
    	RouterModule 
  	]
})
export class AppRoutingModule { }